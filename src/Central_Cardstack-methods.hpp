// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef BIG_M
#define BIG_M
bool Central_Cardstack::attachable(Player* player, Card* c) {
        bool ret;
#ifdef DEBUGOUT
        std::cout << "attachable called" << height + 0 << std::endl;
        if (is_empty(player)) std::cerr << "OOPS" << std::endl;
#endif
        char a = c->get_card_number();
        char b = get_top_card(player)->get_card_number();
#ifdef DEBUGOUT
        std::cout << "Got cardnr" << std::endl;
#endif
        times.compare_two_cards(player);
#ifdef DEBUGOUT
        std::cout << "attachable returns" << std::endl;
#endif
        return ((a - b) % 13 == 1 || (b - a) % 13 == 1);
}
#endif
