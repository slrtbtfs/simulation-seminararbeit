// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef CARD
#define CARD

enum card_color {
        kreuz,
        pik,
        herz,
        karo,
};
enum card_value { A, _2, _3, _4, _5, _6, _7, _8, _9, _10, J, Q, K };
class Card final {
       private:
        card_color color;
        card_value value;
        char string[3];
        bool string_created = false;

       public:
        Card(card_color c, card_value v) {
                color = c;
                value = v;
        }
        char* string_representation() {
                if (!string_created) {
                        char temp[3];
                        *(temp + 2) = ' ';
                        /**
                         * sharp: S;
                         * peak: P;
                         * hearts: H;
                         * check: C;
                         */
                        switch (color) {
                                case 0:
                                        temp[0] = 'S';
                                        break;
                                case 1:
                                        temp[0] = 'P';
                                        break;
                                case 2:
                                        temp[0] = 'H';
                                        break;
                                case 3:
                                        temp[0] = 'C';
                                        break;
                        }

                        switch (value) {
                                case 0:
                                        temp[1] = 'A';
                                        break;
                                case 9:
                                        temp[1] = '1';
                                        temp[2] = '0';
                                        break;
                                case 10:
                                        temp[1] = 'J';
                                        break;
                                case 11:
                                        temp[1] = 'Q';
                                        break;
                                case 12:
                                        temp[1] = 'K';
                                        break;
                                default:
                                        temp[1] = 49 + value;  // The numbers in
                                                               // ASCII start at
                                        // 48.if value==1, we have the card
                                        // value "2", so we add 49
                        }
                        for (int i = 0; i < 3; i++) string[i] = temp[i];
                }

                return string;
        }
        char get_card_number() {
#ifdef DEBUGOUT
                std::cout << "get_card_number called" << std::endl;
#endif
                return value;
        }
        char get_card_color() { return color; }
};
#endif
