// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on

#include "includes.hpp"
#ifndef SMALL_CARDSTACK_M
#define SMALL_CARDSTACK_M

bool Small_Cardstack::attachable(Player* player, Card* c) {
        Card* top_card;
        if (this->is_empty(player)) return true;
        top_card = get_top_card(player);
        if (top_card == 0) return true;
        char a = c->get_card_number();
        char b = top_card->get_card_number();
        times.compare_two_cards(player);
        return a == b;
}
#endif
