// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef SIMULATION
#define SIMULATION

class Simulation {
       public:
        Game *current_game;
        Simulation(int games) {
                Agressive_Player *p0, *p1;
                p0 = new Agressive_Player();
                p1 = new Agressive_Player();
                Simulation(games, p0, p1);
                delete p0;
                delete p1;
        }
        Simulation(int games, Player *player_0, Player *player_1,
                   int *counter) {
#ifndef QUIET
                std::cout << "No Random seed provided, using time\n";
#endif
                Simulation(std::time(0), games, player_0, player_1, counter);
        }
        Simulation(int games, Player *player_0, Player *player_1) {
                int counter;
                Simulation(games, player_0, player_1, &counter);
        }
        Simulation(int rand_seed, int games, Player *player_0, Player *player_1,
                   int *counter);
        Simulation(int rand_seed, int games, Player *player_0,
                   Player *player_1) {
                int counter = 0;
                Simulation(rand_seed, games, player_0, player_1, &counter);
        }
};

#endif
