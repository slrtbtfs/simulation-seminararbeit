// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef SIMULATION_M
#define SIMULATION_M

Simulation::Simulation(int rand_seed, int games, Player* player_0,
                       Player* player_1, int* counter) {
        *counter = 0;
#ifndef QUIET
        std::cout << "Using random seed" << rand_seed << "\n";
#endif
        int game_rand_seed[games];
        std::srand(rand_seed);
        for (int i = 0; i < games; i++) {
                game_rand_seed[i] = std::rand();
#ifndef QUIET
                std::cout << "Created Rand seed " << i << " which is"
                          << game_rand_seed[i] << "\n";
#endif
        }
#ifndef QUIET
        std::cout << "Created Rand seeds\n"
                  << "Starting Games" << std::endl;
#endif
        for (int i = 0; i < games; i++) {
#ifndef QUIET
                std::cout << "Started Game " << i << " with random_seed "
                          << game_rand_seed[i] << std::endl;
#endif
                current_game = new Game(game_rand_seed[i], player_0, player_1);
                if (current_game->winner_id == 0) *counter += 1;
#ifndef QUIET
                std::cout << "counter: " << *counter << std::endl;
#endif
        debug:
                delete current_game;
        }
}
#endif
