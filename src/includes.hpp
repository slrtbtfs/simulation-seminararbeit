// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#ifndef INCLUDES
#define INCLUDES
// This file is included by every other file and makes sure, that
// the files are
// in the right order
using namespace std;

// system libs
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <exception>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>
//#include <rapidxml.hpp>
// class definitions
#include "class-definitions.hpp"

// The classes with method definitions
#include "Card.hpp"
#include "Player.hpp"
#include "Cardstack.hpp"
#include "Small_Cardstack.hpp"
#include "Big_Cardstack.hpp"
#include "Closed_Cardstack.hpp"
#include "Central_Cardstack.hpp"
#include "Dummy_Player.hpp"
#include "Game.hpp"
#include "Agressive_Player.hpp"
#include "Simulation.hpp"
#include "Times.hpp"

// The implementation of the Methods
#include "Agressive_Player-methods.hpp"
#include "Cardstack-methods.hpp"
#include "Central_Cardstack-methods.hpp"
#include "Game-methods.hpp"
#include "Player-methods.hpp"
#include "Simulation-methods.hpp"
#include "Small_Cardstack-methods.hpp"

#endif
