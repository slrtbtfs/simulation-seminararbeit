// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef CARDSTACK_M
#define CARDSTACK_M
Card* Cardstack::take_top_card(Player* player) {
        times.move_one_card(player);
        if (height == 0) return 0;
        height--;
        vomit();
        return stack[height];
}
bool Cardstack::is_empty(Player* player) {
#ifdef DEBUGOUT
        std::cout << "is_empty() called" << std::endl;
#endif
        times.check_if_empty(player);
        return height == 0;
}
Card* Cardstack::get_top_card(Player* player) {
#ifdef DEBUGOUT
        std::cout << "Cardstack::get_top_card called" << std::endl;
#endif
        times.view_one_card(player);
#ifdef DEBUGOUT
        if (height == 0) std::cerr << "Trying to get Card from empty Cardstack";
#endif
        vomit();
        return *(stack + height - 1);
}
#ifdef DEBUGOUT
void Cardstack::vomit() {
        for (int i = 0; i < height; i++) {
                std::cout << stack[i]->string_representation() << " ";
        }
        std::cout << std::endl;
}
#else
void Cardstack::vomit() { return; }
#endif
#endif
