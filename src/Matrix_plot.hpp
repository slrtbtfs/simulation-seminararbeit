// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#ifndef MATRIX_PLOT
#define MATRIX_PLOT

#include <cmath>
#include <simple_svg_1.0.0.hpp>
using namespace svg;
class Matrix_plot {
       private:
        static const int frame_size_x = 50;
        static const int frame_size_y = 50;
        static const int field_size_x = 50;
        static const int field_size_y = 50;
        static const int text_size = 15;
        static const int text_x = 10;
        static const int text_y = 30;
        svg::Dimensions *dimensions;
        svg::Document *doc;

       public:
        Matrix_plot(const char *path, double input_vals[], int input_vals_x,
                    int input_vals_y, int start);
};

Matrix_plot::Matrix_plot(const char *path, double input_vals[],
                         int input_vals_x, int input_vals_y, int start) {
        dimensions =
            new svg::Dimensions(frame_size_x + field_size_x * input_vals_x,
                                frame_size_y + field_size_y * input_vals_y);
        doc = new svg::Document(path,
                                svg::Layout(*dimensions, svg::Layout::TopLeft));
        // Image Border
        svg::Polygon border(svg::Stroke(1, svg::Color::Black));
        border << Point(0, 0) << Point(dimensions->width, 0)
               << Point(dimensions->width, dimensions->height)
               << Point(0, dimensions->height);
        *doc << border;

        // plot
        int index = 0;
        for (int i = 0; i < input_vals_x; i++)
                for (int j = 0; j < input_vals_y; j++) {
                        int c = input_vals[index] * 255;
                        *doc << Rectangle(
                            Point(frame_size_x + field_size_x * i,
                                  frame_size_y + field_size_y * j),
                            field_size_x, field_size_y, Color(c, c, c));
                        index++;
                }
        // write text
        index = 0;
        for (int i = 0; i < input_vals_x; i++)
                for (int j = 0; j < input_vals_y; j++) {
                        int c = input_vals[index] < 0.5 ? 255 : 0;
                        *doc << Text(
                            Point(frame_size_x + field_size_x * i + text_x,
                                  frame_size_y + field_size_y * j + text_y),
                            (std::to_string((int)(input_vals[index] * 100)) +
                             "\%")
                                .c_str(),
                            Color(c, c, c), Font(text_size, "SourceSans"));
                        index++;
                }
        for (int i = 0; i < input_vals_x; i++) {
                *doc << Text(
                    Point(frame_size_x + field_size_x * i + text_x, text_y),
                    (std::to_string(i + start)).c_str(), Color(0, 0, 0),
                    Font(text_size, "SourceSans"));
        }

        for (int i = 0; i < input_vals_y; i++) {
                *doc << Text(
                    Point(text_x, frame_size_y + field_size_y * i + text_y),
                    (std::to_string(i + start)).c_str(), Color(0, 0, 0),
                    Font(text_size, "SourceSans"));
        }
        doc->save();
        delete doc;
        delete dimensions;
}

#endif
