// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
    *    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef GAME_M
#define GAME_M
// private
int Game::winner() {
        for (int i = 0; i < 2; i++) {
                if (players[i].closed.is_empty(dummy_player)) {
                        for (int j = 0; j < 4; j++) {
                                if (!players[i].open[j].is_empty(dummy_player))
                                        goto not_the_winner;
                        }
                        winner_id = i;
                        return i;
                not_the_winner:;
                }
        }           // else
        return -1;  // no winner
}
void Game::mainloop() {
#ifdef DEBUGOUT
        std::cout << "Mainloop started" << std::endl;
#endif
        start();
        int i = 0;
        // the player, that did the last action
        int turn = 0;

        while (winner() == -1) {
#ifdef DEBUGOUT
                std::cout << "New Mainloop round started" << std::endl;
#endif
                if (i++ > 10) {
#ifdef DEBUGOUT
                        std::cout << "nothing_to_do called" << std::endl;
#endif

                        i = 0;
                        if (nothing_to_do()) start();
#ifdef DEBUGOUT
                        std::cout << "nothing_to_do returns" << std::endl;
#endif
                }
#ifdef DEBUGOUT
                std::cout << "Action should happen now" << std::endl;
#endif

                if (players[(turn + 1) % 2].actor->time <=
                    players[turn].actor->time)
                        turn = (turn + 1) % 2;

                players[turn].actor->action();
        }
#ifndef QUIET
        std::cout << winner() << " won" << std::endl;
#endif
}
// public
Game::Game(int random_seed, Player* player_0, Player* player_1) {
        dummy_player = &_dummy_player;
#ifdef DEBUGOUT
        std::cout << "Started Game" << std::endl;
#endif
        srand48(random_seed);
        // Create the cards
        for (int color = 0; color < 4; color++) {
                for (int value = 0; value < 13; value++) {
                        int index = color * 13 + value;
                        created_cards[index] =
                            new Card((card_color)color, (card_value)value);
                        players[0].closed.append(**(created_cards + index));
                }
        }
#ifdef DEBUGOUT
        std::cout << "Created Cards" << std::endl;
#endif

        // shuffle them
        players[0].closed.shuffle();

#ifdef DEBUGOUT
        std::cout << "Shuffled Cards" << std::endl;
#endif
        // and share them between the players
        for (int i = 0; i <= 25; i++) {
                Card* c = players[0].closed.take_top_card(dummy_player);
                players[1].closed.append(*c);
        }

#ifdef DEBUGOUT
        std::cout << "Shared Cards" << std::endl;
#endif
        players[0].actor = player_0;
        players[1].actor = player_1;
        players[0].actor->set_id(0);
        players[1].actor->set_id(1);
        players[0].actor->set_game(this);
        players[1].actor->set_game(this);

        fill_up(dummy_player, 0);
        fill_up(dummy_player, 1);
        mainloop();
}

bool Game::is_gin(Player* player) {
        if (central[0].is_empty(player) || central[1].is_empty(player))
                return false;
        else if (central[0].get_top_card(player)->get_card_number() ==
                 central[1].get_top_card(player)->get_card_number()) {
                Central_Cardstack *bigger_stack, *smaller_stack;
                int id = player->id;
                if (central[1].height > central[0].height) {
                        bigger_stack = central + 1;
                        smaller_stack = central;
                } else {
                        bigger_stack = central;
                        smaller_stack = central + 1;
                }
                players[id].closed.append(dummy_player, smaller_stack);
                players[(id + 1) % 2].closed.append(dummy_player, bigger_stack);
                bigger_stack->shuffle();
                smaller_stack->shuffle();
                cout << "gin" << endl;
                start();
                return true;
        } else
                return false;
}
void Game::fill_up(Player* player, int player_id /*, int max_time*/) {
#ifdef DEBUGOUT
        std::cout << "Called fill_up" << std::endl;
#endif
        Card* closed_top;
        bool attached_a_card = true;
        bool empty_stacks;

        while (attached_a_card) {
                if (players[player_id].closed.is_empty(player))
                        return;
                else
                        closed_top =
                            players[player_id].closed.get_top_card(player);
                attached_a_card = false;
                // test if there are empty stacks
                empty_stacks = false;
                for (int i = 0; i < 4; i++)
                        if (players[player_id].open[i].is_empty(dummy_player)) {
                                empty_stacks = true;
                                break;
                        }
                if (!empty_stacks) return;

                for (int i = 0; i < 4; i++) {
                        if (players[player_id].open[i].attachable(player,
                                                                  closed_top) &&
                            !players[player_id].open[i].is_empty(player)) {
                                players[player_id].open[i].append(
                                    *players[player_id].closed.take_top_card(
                                        player));
                                attached_a_card = true;
                                break;
                        }
                }
                for (int i = 0; i < 4; i++) {
                        if (!attached_a_card &&
                            players[player_id].open[i].is_empty(player)) {
                                players[player_id].open[i].append(
                                    *players[player_id].closed.take_top_card(
                                        player));
                                attached_a_card = true;
                                break;
                        }
                }
        }
#ifdef DEBUGOUT
        std::cout << "fill_up returns" << std::endl;
#endif
}
void Game::start() {
#ifdef DEBUGOUT
        std::cout << "Called Game::start" << std::endl;
#endif
        for (int i = 0; i < 2; i++) {
                players[i].actor->time = 0;
                // if this player has cards on his/her closed stack
                if (!players[i].closed.is_empty(dummy_player))
                        central[i].append(
                            *(players[i].closed.take_top_card(dummy_player)));
                // if the other one has
                else if (!players[(i + 1) % 2].closed.is_empty(dummy_player))
                        central[i].append(
                            *(players[(i + 1) % 2].closed.take_top_card(
                                dummy_player)));
                // if no one has
                else    // bring a random card of the cardstack to the
                        // top
                        central[i].shuffle();
        }
#ifdef DEBUGOUT
        std::cout << "Game::start returned" << std::endl;
#endif
}
bool Game::nothing_to_do() {
        for (int i = 0; i < 2; i++)
                for (int j = 0; j < 4; j++)
                        for (int k = 0; k < 2; k++)
                                if ((!players[i].open[j].is_empty(
                                        dummy_player)) &&
                                        central[k].attachable(
                                            dummy_player,
                                            (players[i].open[j].get_top_card(
                                                dummy_player))) ||
                                    (!players[i].closed.is_empty(
                                         dummy_player) &&
                                     players[i].open[j].is_empty(dummy_player)))
                                        return false;
// else
#ifdef DEBUGOUT
        std::cout << "nothing to do!" << std::endl;
#endif
        return true;
}
#endif
Game::~Game() {
        for (int i = 0; i < 52; i++) {
                if (i < 12) delete stacks[i]->stack;
                delete created_cards[i];
        }
}
