// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef BIG_CARDSTACK
#define BIG_CARDSTACK
class Big_Cardstack : public Cardstack {
       public:
        Big_Cardstack() : Cardstack(52){};

        void shuffle() {
#ifdef DEBUGOUT
                std::cout << "Called shuffle" << std::endl;
#endif
                for (int i = height - 1; i > 0; i--) {
                        int new_position = (int)(drand48() * (i + 1));
#ifdef DEBUGOUT
                        std::cout << "Old Card pos" << i << std::endl;
                        std::cout << "New Card pos" << new_position
                                  << std::endl;
#endif
                        swap(i, new_position);
                }
                vomit();
        }

       private:
        void swap(int a, int b) {
                Card* tmp = stack[a];
                stack[a] = stack[b];
                stack[b] = tmp;
        }
};
#endif
