// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef AGRESSIVE_PLAYER_M
#define AGRESSIVE_PlAYER_M
void Agressive_Player::action() {
        int counter = 0;
        int gin_counter;
        bool success = false;
#ifdef DEBUGOUT
        std::cout << "called action" << std::endl;
#endif
        switch (phase) {
                case fill_up:
#ifdef DEBUGOUT
                        std::cout << "Phase: fill up" << std::endl;
#endif
                        game->fill_up(this, id);
                        phase = put_cards;
                        to_check = 0;
                        failed_attempts = 0;
                        break;

                case put_cards:
#ifdef DEBUGOUT
                        std::cout << "Phase: put cards " << to_check
                                  << std::endl;
#endif
                        // go to the next open stack with cards
                        while (
                            game->players[id].open[to_check].is_empty(this)) {
#ifdef DEBUGOUT
                                std::cout << "Cardstack empty" << to_check
                                          << std::endl;
#endif
                                increment_to_check();
                                counter++;
                                // if no more cards are there
                                if (counter > 3) {
                                        phase = fill_up;
                                        return;
                                }
                        }

                        for (int i = 0; i < 2; i++) {
                                if (game->central[i].attachable(
                                        this, game->players[id]
                                                  .open[to_check]
                                                  .get_top_card(this))) {
#ifdef DEBUGOUT
                                        std::cout << "Check succeded"
                                                  << std::endl;
#endif
                                        game->central[i].append(
                                            *(game->players[id]
                                                  .open[to_check]
                                                  .take_top_card(this)));
                                        success = true;
                                        failed_attempts = 0;
                                        break;
                                }
                        }
                        if (!success) failed_attempts++;
                        if (failed_attempts > tries) phase = fill_up;
                        increment_to_check();
                        // check for gin
                        if (gin_check > 0) {
                                gin_count++;
                                if ((gin_count) == gin_check) {
                                        game->is_gin(this);
                                        gin_count = 0;
                                }
                        }
        }
}
void Agressive_Player::increment_to_check() {
        if (++to_check > 3) to_check = 0;
}

#endif
