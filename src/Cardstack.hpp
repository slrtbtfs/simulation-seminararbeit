// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on

#include "includes.hpp"
#ifndef CARDSTACK
#define CARDSTACK
class Cardstack {
       protected:
        char size;

       public:
        char height = 0;
        Card** stack;

        Cardstack(char stack_size) {
                size = stack_size;
                stack = new Card*[size];
                height = 0;
        }

        Card* take_top_card(Player* player);
        Card* get_top_card(Player* player);
        void append(Card& new_top) {
#ifdef DEBUGOUT
                std::cout << "append:" << height + 0 << std::endl;
                if (height >= size) {
                        std::cerr << "Cardstack::append: cardstack full";
                        std::terminate();
                }
#endif
                stack[height] = &new_top;
                height++;
                vomit();
        }

        bool is_empty(Player* player);
        virtual bool attachable(Player player, Card* c) { return false; };
        void append(Player* player, Cardstack* to_append) {
                for (int i = 0; i < to_append->height; i++)
                        append(*to_append->take_top_card(player));
        }
        void vomit();
};
#endif
