// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on

#ifndef GIN_MATRIX
#define GIN_MATRIX
#include "Agressive_Player.hpp"
#include "Matrix_plot.hpp"
#include "Simulation.hpp"
#define GAMES 10
#define MATRIX_SIZE 10
#define MATRIX_MIN 1

int main() {
        Agressive_Player* player_0 = new Agressive_Player();
        Agressive_Player* player_1 = new Agressive_Player();
        static const int max = MATRIX_SIZE + MATRIX_MIN;
        int matrix[MATRIX_SIZE * MATRIX_SIZE];
        double m[MATRIX_SIZE * MATRIX_SIZE];
        int index = 0;
        Simulation* current;
        for (int i = MATRIX_MIN; i < max; i++) {
                player_0->gin_check = i;
                for (int j = MATRIX_MIN; j < max; j++) {
                        player_1->gin_check = j;
                        current = new Simulation(GAMES, player_0, player_1,
                                                 matrix + index);
                        index++;
                        delete current;
                        cout << "Progress: "
                             << ((int)((double)100 * index /
                                       (MATRIX_SIZE * MATRIX_SIZE)))
                             << "%  ";
                        for (int k = 0;
                             k < 70 * index / (MATRIX_SIZE * MATRIX_SIZE); k++)
                                cout << "#";
                        cout << "\r" << flush;
                }
        }
        for (int i = 0; i < MATRIX_SIZE * MATRIX_SIZE; i++)
                m[i] = (double)matrix[i] / GAMES;
        new Matrix_plot("gin-matrix.svg", m, MATRIX_SIZE, MATRIX_SIZE,
                        MATRIX_MIN);
}

#endif
