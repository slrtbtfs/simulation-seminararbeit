// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef TIMES
#define TIMES
static class Times {
        int t_move_one_card = 5;
        int t_compare_two_cards = 2;
        int t_view_one_card = 2;
        int t_check_if_empty = 1;

       public:
        void move_one_card(Player* player) { player->time += t_move_one_card; }
        void compare_two_cards(Player* player) {
#ifdef DEBUGOUT
                std::cout << "Called compare_two_cards" << std::endl;
#endif
                player->time += t_compare_two_cards;
#ifdef DEBUGOUT
                std::cout << "compare_two_cards returns" << std::endl;
#endif
        }
        void view_one_card(Player* player) { player->time += t_view_one_card; }
        void check_if_empty(Player* player) {
                player->time += t_check_if_empty;
        }
} times;
#endif
