#!/usr/bin/env python
#     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
# with the topic ''Untersuchung von schnellen Entscheidungen in
# dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.

#
#    This is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This software is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with This Software.  If not, see <http://www.gnu.org/licenses/>.

from bigfloat import *
import numpy as np
# A list with all factorials
with precision(100):
    factorials = np.array([BigFloat(0), BigFloat(1)])
    pValues = np.empty(shape=(0), dtype=BigFloat)
    for i in range(2, 100001):
        # get the next factorial
        factorials = np.append(factorials,
                               [BigFloat(factorials[-1] * i)])
        print "calculating factorial of " + str(i)
        print factorials[-1]

    def P(k):
        print "calculating P(" + str(k) + ")"
        binkof = BigFloat(BigFloat(factorials[100000])
                          / BigFloat(factorials[k] * factorials[100000 - k]))
        ret = BigFloat(binkof * BigFloat(0.5)**BigFloat(k)
                       * BigFloat(0.5)**BigFloat(100000 - k))
        print ret
        return ret

    for i in range(0, 100001):
        pValues = np.append(pValues, [BigFloat(P(i))])

    # The Programm does not manage, to calculate P(0) and
    # P(100000) correctly (probably lack of precision).
    # Since they are much smaller than 10^-30000, there
    # is no harm done, if we set them to zero
    pValues[0] = BigFloat(0)
    pValues[100000] = BigFloat(0)

    def Q(a, b):
        ret = BigFloat(0)
        for i in range(a, b + 1):
            ret += pValues[i]
        return ret

    for i in range(1, 20):
        b = int(0.5 + 0.5**i * 100000)
        print "P(Abweichung vom Erwartungswert <" \
            + str(0.5**i * 100) + "%)"
        print "Q(50000-" + str(b) + ", 50000+" + str(b) + ") = " \
            + str(Q(50000 - b, 50000 + b))
