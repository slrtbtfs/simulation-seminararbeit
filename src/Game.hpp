// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on
#include "includes.hpp"
#ifndef GAME
#define GAME

class Game {
       public:
        struct players_cards {
                Closed_Cardstack closed;
                Small_Cardstack open[4];
                Player *actor;
        } players[2];
        Dummy_Player _dummy_player;
        // points to above
        Dummy_Player *dummy_player;

        Central_Cardstack central[2];
        int winner_id;

       private:
        Card *created_cards[52];
        Cardstack *stacks[12] = {
            &players[0].closed,  &players[0].open[0], &players[0].open[1],
            &players[0].open[2], &players[0].open[3], &central[0],
            &players[1].closed,  &players[1].open[0], &players[1].open[1],
            &players[1].open[2], &players[1].open[3], &central[1]};

        int winner();
        void mainloop();
        bool nothing_to_do();

       public:
        Game(int random_seed, Player *player_1, Player *player_2);
        bool is_gin(Player *player);
        void fill_up(Player *player, int player_id /*, int max_time*/);
        void start();
        ~Game();
};

#endif
