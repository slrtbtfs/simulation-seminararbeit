// clang-format off
/*
*     This file is part of the ``Seminararbeit`` written by Tobias Guggenmos
*     with the topic ''Untersuchung von schnellen Entscheidungen in dynamischen Situationen anhand eines Zwei-Personen-Kartenspiels.
*
*
*    This is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with This Software.  If not, see <http://www.gnu.org/licenses/>.
*/
// clang-format on

#ifndef TRIES_GRAPH
#define TRIES_GRAPH
#define QUIET
#include "Simulation.hpp"
#define GAMES 100000
#define MATRIX_SIZE 100
#define MATRIX_MIN 3

void show_progress(int);

int main() {
        Agressive_Player* player_0 = new Agressive_Player();
        Agressive_Player* player_1 = new Agressive_Player();
        Simulation* current;
        int progress = 0;
        int won[3];
        int max = MATRIX_MIN + MATRIX_SIZE;
        ofstream output_file;
        output_file.open("tries-graph.csv");
        for (int i = MATRIX_MIN; i < max; i++) {
                show_progress(progress);
                player_0->tries = i;
                player_1->tries = 3;
                current = new Simulation(GAMES, player_0, player_1, won);
                delete current;
                player_1->tries = 15;
                current = new Simulation(GAMES, player_0, player_1, won + 1);
                delete current;
                player_1->tries = 50;
                current = new Simulation(GAMES, player_0, player_1, won + 2);
                delete current;
                output_file << i << "," << (double)won[0] / GAMES << ","
                            << (double)won[1] / GAMES << ","
                            << (double)won[2] / GAMES << "\n";
                progress++;
        }
        output_file.close();
}
void show_progress(int progress) {
        cout << "Progress: " << progress << "%  ";
        for (int k = 0; k < 70 * progress / 100; k++) cout << "#";
        cout << "\r" << flush;
}
#endif
